$("document").ready(function() {

	$("#nav-button").on("click", toggleNavDropDown);

	function toggleNavDropDown() {
		$("#nav-dropdown-links").slideToggle("fade");
	}

	$(window).resize(function() {
		var win = $(this);
		if(win.width() > 800) {
			$("#nav-dropdown-links").hide();
			$("#nav-brand-text").text("Every Ding Every Dong");
		}
		else if(win.width() < 800) {
			$("#nav-brand-text").text("EDED");
		}
	})
})

function populateNovelTable() {
	const table = document.getElementById("novelTable");

	books = {
		"Terminal Man" : 9,
		"Rosemary's Baby" : 8,
		"Psycho" : 8,
		"1984" : "",
		"Fahrenheit 451" : "" ,
		"The Sphere" : 10,
		"Four Past Midnight" : 10,
		"Insomnia" : "",
		"Treasure Island" : "",
		"Hitchhiker's Guide to the Galaxy" : "",
		"Cosmos" : "",
	};

	for(let key in books)
	{
		let row = table.insertRow();
		let novel = row.insertCell(0);
		let rating = row.insertCell(1);
		novel.innerHTML = key;
		if(books[key] != "")
			rating.innerHTML = books[key] + " / 10";
		else
			rating.innerHTML = "Yet to start reading";
	}
}
