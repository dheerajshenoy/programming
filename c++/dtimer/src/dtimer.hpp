#include <qt/QtWidgets/QMainWindow>
#include <qt/QtWidgets/QApplication>
#include <qt/QtWidgets/QWidget>
#include <qt/QtWidgets/QVBoxLayout>
#include <qt/QtWidgets/QHBoxLayout>
#include <qt/QtWidgets/QSpinBox>
#include <qt/QtWidgets/QPushButton>
#include <qt/QtWidgets/QLabel>
#include <qt/QtWidgets/QProgressBar>
#include <qt/QtWidgets/QTimeEdit>
#include <qt/QtCore/QTime>
#include <qt/QtCore/QTimer>
#include <qt/QtCore/QDir>
#include <qt/QtCore/QFile>
#include <qt/QtGui/QFont>
#include <qt/QtCore/QDebug>
#include <allegro5/allegro_audio.h>
#include <qt/QtConcurrent/QtConcurrent>
#include <qt/QtCore/QFuture>

class MainWindow : public QMainWindow
{
public:
  QWidget *mainWidget = new QWidget();
  QVBoxLayout *mainLayout = new QVBoxLayout();
  QHBoxLayout *btnLayout = new QHBoxLayout();
  QLabel *timeLabel = new QLabel();
  QPushButton *btnStartTimer = new QPushButton();
  QPushButton *btnPauseTimer = new QPushButton();
  QPushButton *btnResumeTimer =new QPushButton();
  QPushButton *btnStopTimer =new QPushButton();
  QProgressBar *timeProgressBar = new QProgressBar();
  QTimer *Timer = new QTimer();
  QFuture<void> *future = new QFuture<void>();
  QTime Time;
  QFont timeLabelFont;
  QFont TimeInputFont;
  QSpinBox *TimeH = new QSpinBox();
  QSpinBox *TimeM = new QSpinBox();
  QSpinBox *TimeS = new QSpinBox();
  QHBoxLayout *timeInputLayout = new QHBoxLayout();
  
  MainWindow(int argc, char* argv[]);
  ~MainWindow();

private:
  bool _isPaused = false;
  void _setupInputBox();
  void _setupButtons();
  void _startTimer();
  void _pauseTimer();
  void _resumeTimer();
  void _stopTimer();
  void _timeDone();
  ulong _timeToMilli(uint);
  void _updateTimeLabel();
  ulong _GetTimeFromInput();
};
