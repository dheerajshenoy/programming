#include "dtimer.hpp"
#include "qabstractspinbox.h"
#include "qnamespace.h"

MainWindow::MainWindow(int argc, char* argv[]) {
  _setupInputBox();
  _setupButtons();
  
  mainWidget->setLayout(mainLayout);

  mainLayout->addWidget(timeLabel);
  mainLayout->addWidget(timeProgressBar);
  mainLayout->addLayout(btnLayout);

  timeLabelFont.setFamily("Roboto Mono");
  timeLabelFont.setPixelSize(40);
  timeLabel->setAlignment(Qt::AlignCenter);

  Time.setHMS(TimeH->value(), TimeM->value(), TimeS->value());
  timeLabel->setFont(timeLabelFont);
  timeLabel->setText(Time.toString());
  this->setCentralWidget(mainWidget);

  Timer->setTimerType(Qt::CoarseTimer);
  Timer->setSingleShot(true);
  connect(Timer, &QTimer::timeout, this, &MainWindow::_timeDone);

  timeProgressBar->hide();
  timeLabel->hide();
}

void MainWindow::_setupButtons() {
  btnLayout->addWidget(btnStartTimer);
  btnLayout->addWidget(btnPauseTimer);
  btnLayout->addWidget(btnResumeTimer);
  btnLayout->addWidget(btnStopTimer);

  btnPauseTimer->hide();
  btnResumeTimer->hide();

  btnStartTimer->setText("START");
  btnStopTimer->setText("RESET");
  btnResumeTimer->setText("RESUME");
  btnPauseTimer->setText("PAUSE");
  btnStopTimer->setEnabled(false);

  connect(btnStartTimer, &QPushButton::released, this, &MainWindow::_startTimer);
  connect(btnPauseTimer, &QPushButton::released, this, &MainWindow::_pauseTimer);
  connect(btnResumeTimer, &QPushButton::released, this, &MainWindow::_resumeTimer);
  connect(btnStopTimer, &QPushButton::released, this, &MainWindow::_stopTimer);
}

void MainWindow::_setupInputBox() {
  timeInputLayout->addWidget(TimeH);
  timeInputLayout->addWidget(TimeM);
  timeInputLayout->addWidget(TimeS);

  TimeH->setAlignment(Qt::AlignCenter);
  TimeM->setAlignment(Qt::AlignCenter);
  TimeS->setAlignment(Qt::AlignCenter);

  TimeH->setButtonSymbols(QAbstractSpinBox::NoButtons);
  TimeM->setButtonSymbols(QAbstractSpinBox::NoButtons);
  TimeS->setButtonSymbols(QAbstractSpinBox::NoButtons);

  TimeInputFont.setFamily("Roboto Mono");
  TimeInputFont.setPixelSize(40);
  
  TimeH->setFont(TimeInputFont);
  TimeM->setFont(TimeInputFont);
  TimeS->setFont(TimeInputFont);
  
  TimeH->setMinimum(0);
  TimeH->setMaximum(100);
  TimeH->setValue(0);

  TimeM->setMinimum(0);
  TimeM->setMaximum(100);
  TimeM->setValue(0);

  TimeS->setMinimum(0);
  TimeS->setMaximum(100);
  TimeS->setValue(0);
  
  mainLayout->addLayout(timeInputLayout);
}

inline ulong MainWindow::_timeToMilli(uint secs) { return secs * 1000; }

ulong MainWindow::_GetTimeFromInput() {
  uint s = TimeS->value();
  uint m = TimeM->value();
  uint h = TimeH->value();

  if(s == 0 && m == 0 && h == 0)
  qDebug() << "H:" << h << " M:" << m << " S:" << s;

  Time.setHMS(h, m, s);
  return _timeToMilli(Time.second());
}

void MainWindow::_startTimer() {
  Timer->setInterval(_GetTimeFromInput());
  Timer->start();
  *future = QtConcurrent::run(this, &MainWindow::_updateTimeLabel);
  timeProgressBar->show();
  TimeH->hide();
  TimeM->hide();
  TimeS->hide();
  timeLabel->show();
  btnStartTimer->hide();
  btnPauseTimer->show();
}

void MainWindow::_pauseTimer() {
  future->pause();
  btnPauseTimer->hide();
  if(Time != QTime(0, 0, 0))
	{
	  btnResumeTimer->show();
	  btnStopTimer->setEnabled(true);
	}
  else
	btnStartTimer->show();
}

void MainWindow::_resumeTimer() {
  btnResumeTimer->hide();
  if(Time != QTime(0, 0, 0))
	{
	  btnPauseTimer->show();
	  btnStopTimer->setEnabled(false);
	  *future = QtConcurrent::run(this, &MainWindow::_updateTimeLabel);
	}
  else
	btnStartTimer->show();
}

void MainWindow::_updateTimeLabel() {
  while(!future->isPaused())
	{
	  Time = Time.addSecs(-1);
	  timeLabel->setText(Time.toString());
	  sleep(1);
	}
}

void MainWindow::_stopTimer() {
  Timer->stop();
  future->cancel();
  btnStartTimer->show();
  btnResumeTimer->hide();
  btnPauseTimer->hide();
  timeLabel->hide();
  TimeH->show();
  TimeM->show();
  TimeS->show();
}

void MainWindow::_timeDone() {
  btnStopTimer->setEnabled(false);
  timeLabel->hide();
  TimeH->show();
  TimeM->show();
  TimeS->show();
}

MainWindow::~MainWindow() {
}
