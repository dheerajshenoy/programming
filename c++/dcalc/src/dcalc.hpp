#include <qt/QtWidgets/QMainWindow>
#include <qt/QtWidgets/QApplication>
#include <qt/QtWidgets/QLineEdit>
#include <qt/QtWidgets/QPushButton>
#include <qt/QtWidgets/QVBoxLayout>
#include <qt/QtWidgets/QHBoxLayout>
#include <qt/QtWidgets/QTableView>
#include <qt/QtWidgets/QWidget>
#include <qt/QtCore/QStringListModel>
#include <qt/QtCore/QDebug>
#include <qt/QtCore/QModelIndex>
#include <qt/QtGui/QFont>
#include <qt/QtWidgets/QHeaderView>

class DCalc : public QMainWindow {

public:

  // WIDGETS
  QTableView *resultList = new QTableView();
  QWidget *mainWidget = new QWidget();
  QVBoxLayout *mainLayout = new QVBoxLayout();
  QLineEdit *inputBar = new QLineEdit();
  QStandardIt *resultListModel = new QStringListModel();

  DCalc();
  ~DCalc();

private:
  void getResult();
  QString Evaluate();
  QFont inputBarFont, resultListFont;
  QStringList _results;

};
