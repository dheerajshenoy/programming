#include "dcalc.hpp"

DCalc::DCalc() {

  inputBarFont.setFamily("JetBrainsMono Nerd Font Mono");
  inputBarFont.setPixelSize(17);
  inputBar->setFont(inputBarFont);

  connect(inputBar, &QLineEdit::returnPressed, this, &DCalc::getResult);

  resultListFont.setFamily("JetBrainsMono Nerd Font Mono");
  resultListFont.setPixelSize(17);
  resultList->setFont(resultListFont);

  resultList->setModel(resultListModel);
  QHeaderView *h = resultList->horizontalHeader();
  h->setStretchLastSection(true);
  h->setSectionResizeMode(QHeaderView::Stretch);
  resultList->setHorizontalHeader(h);

  mainLayout->addWidget(resultList);
  mainLayout->addWidget(inputBar);
  mainWidget->setLayout(mainLayout);
  this->setCentralWidget(mainWidget);
}

void DCalc::getResult() {
  resultListModel->insertRow(0);
  resultListModel->insertColumn(0);
  resultListModel->insertColumn(1);
  QModelIndex m1 = resultListModel->index(0, 0);
  resultListModel->setItem
  resultListModel->setData(m1, inputBar->text());
  QModelIndex m2 = resultListModel->index(0, 1);
  resultListModel->setData(m2, Evaluate());
}

QString DCalc::Evaluate() {

  return "DD";
}

DCalc::~DCalc() {}
