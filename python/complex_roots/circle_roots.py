import matplotlib.pyplot as plt
import numpy as np
from numpy import sin, cos, sqrt, pi
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation

Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

R = 1
r = 0.04

fig, ax = plt.subplots()
ax.set_aspect('equal')

ax.set_xlim(-R-1, R+1)
ax.set_ylim(-R-1, R+1)

ax.add_artist(plt.Circle((0, 0), R, fill=False, color='k'))

def circle(theta):
    return np.array([R * cos(theta), R * sin(theta)])

cs = []
circles = 150
ck = plt.Circle((0, 0), r, color='k')
cs.append(ck)
ax.add_artist(ck)


for i in range(1, circles+3):
    ck = plt.Circle((0, 0), r, color='b')
    cs.append(ck)
    ax.add_artist(ck)

X = np.linspace(-R, R, 100)
M = 0.5
C1 = 0

plt.plot(X, np.zeros(len(X)), 'k')
plt.plot(np.zeros(len(X)), X, 'k')

L = np.linspace(-3*pi, 3*pi, circles)
for ms in L:
    plt.plot(X, ms*X, 'k', alpha=0)

def animation(i):
    x, y = circle(i)
    cs[0].center = (x, y)
    cs[1].center = (0, y)
    cs[2].center = (x, 0)
    for ms in range(len(L)):
        M = L[ms]
        C2 = y + x/M
        X = M*(C2-C1)/(1+M**2)
        Y = (C1 + M**2 * C2)/(M**2 + 1)
        cs[ms+3].center = (X, Y)

    return cs


anim = FuncAnimation(fig, animation, frames=np.linspace(0, 2*pi, 360), interval=0.01)
#plt.grid()
#anim.save('im4.mp4', writer=writer)
plt.show()
