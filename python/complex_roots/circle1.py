import matplotlib.pyplot as plt
import numpy as np
from numpy import pi, sqrt, sin, cos
import random as rand
import sys
import getopt

r = 10
n = 10
DRAW_POINT = True
DRAW_BIG_CIRCLE = True
DRAW_SMALL_CIRCLE = True

try:
    opts, args = getopt.getopt(sys.argv[1:], 'r:n:', ['no-point', 'no-big', 'no-small'])
except getopt.GetoptError as err:
    pass
for o, a in opts:
    if o == '-r':
        r = float(a)
    elif o == '-n':
        n = float(a)
    elif o == '--no-point':
        DRAW_POINT = False
    elif o == '--no-big':
        DRAW_BIG_CIRCLE = False
    elif o == '--no-small':
        DRAW_SMALL_CIRCLE = False

R = (n * r)/ pi
R = round(R, 2)

colors = ['r', 'k', 'b', 'c', 'm']
fig, ax = plt.subplots()
ax.set_aspect('equal')
plt.xlim(-R - r - 2, R + r + 2)
plt.ylim(-R - r - 2, R + r + 2)

if DRAW_BIG_CIRCLE:
    C0 = plt.Circle((0, 0), R, fill=False, color='b')
    ax.add_artist(C0)

theta = 0 
i = 0

while i < n:
    theta = theta + 2 * pi / n
    x = R * cos(theta)
    y = R * sin(theta)
    if DRAW_SMALL_CIRCLE:
        cn = plt.Circle((x, y), r, fill=False, color=rand.choice(colors))
        ax.add_artist(cn)
    if DRAW_POINT:
        plt.plot(x, y, '.k')
    i += 1

plt.title("R = {} Units\nr = {} Units\nn = {}".format(R, r, n))
plt.grid()
plt.show()

