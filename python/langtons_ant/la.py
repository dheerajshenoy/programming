import turtle
import random as rand
import numpy as np
from numpy import sin, cos, pi

maps = {}
colors = ["white", "black"]

def langton(maps):

    window = turtle.Screen()
    window.bgcolor('white')
    window.screensize(10000, 10000)
    window.tracer(False)
    
    s = turtle.getscreen()
    ant = turtle.Turtle()
    ant.shape('square')
    ant.shapesize(0.5)
    ant.speed('fastest')
    
    t = np.linspace(0, 2*pi, 360)
    r = 10
    for i in t:
        x, y = r * sin(i), r * cos(i)
        maps[(x, y)] = "black"
        ant.fillcolor("black")
        ant.stamp()
        invert(maps, ant, "black")
    print(maps)

    pos = coor(ant)
    i=0
    while i<30000:
        step = 10
        if pos not in maps or maps[pos] == "white":
            ant.fillcolor("black")
            ant.stamp()
            invert(maps, ant, "black")
            ant.right(90)
            ant.forward(step)
            pos = coor(ant)

        elif maps[pos] == "black":
            ant.fillcolor("white")
            invert(maps, ant, "white")
            ant.stamp()
            ant.left(90)
            ant.forward(step)
            pos = coor(ant)
        i += 1
    window.tracer(True)
    turtle.done()

def invert(graph, ant, color):
    graph[coor(ant)] = color

def coor(ant):
    return (round(ant.xcor()), round(ant.ycor()))

langton(maps)
