import numpy as np
import matplotlib.pyplot as plt
import random as rand
import pandas as pd
import seaborn as sb

X, Y = [], []
N = 100

for i in range(N):
    X.append(i)
    Y.append(rand.random())
df = pd.DataFrame({"x": X, "y": Y})
print(X, Y)
sb.displot(df, x='x', y='y')
plt.show()
