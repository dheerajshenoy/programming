import numpy as np


m, n = 3, 3
MAX = 4
a = np.zeros([m, n])

a[0, 0] = 6
a[1, 1] = 6
a[2, 2] = 6

print()
def Print(a):
    for i in range(len(a)):
        print(a[i])
    print("\n")

def checkIfMaxElement(arr, MAX):
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if(arr[i][j] >= MAX):
                return True
    return False

def sandPile(arr, MAX):
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if arr[i][j] >= MAX:

                # First cell
                if i == 0 and j == 0:
                    arr[i][j+1] = arr[i][j+1] + 1
                    arr[i+1][j] = arr[i+1][j] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue

                # First column
                elif i > 0 and j == 0 and i < m-1:
                    arr[i][j+1] = arr[i][j+1] + 1
                    arr[i+1][j] = arr[i+1][j] + 1
                    arr[i-1][j] = arr[i-1][j] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue

                # first row
                elif i == 0 and j < n-1:
                    arr[i][j+1] = arr[i][j+1] + 1
                    arr[i][j-1] = arr[i][j-1] + 1
                    arr[i+1][j] = arr[i+1][j] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue

                # last cell of the first row, first col
                elif i == (m-1) and j == 0:
                    arr[i][j+1] = arr[i][j+1] + 1
                    arr[i-1][j] = arr[i-1][j] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue
                
                # row, col except last ones
                elif i > 0 and i < m-1 and j > 0 and j < n-1:
                    arr[i][j+1] = arr[i][j+1] + 1
                    arr[i][j-1] = arr[i][j-1] + 1
                    arr[i-1][j] = arr[i-1][j] + 1
                    arr[i+1][j] = arr[i+1][j] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue
                
                # any row, last col
                elif i > 0 and j == n-1:
                    arr[i][j-1] = arr[i][j-1] + 1
                    arr[i-1][j] = arr[i-1][j] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue
                
                # last row, last col
                elif i == m-1 and j > 0 and j < n-1:
                    arr[i][j-1] = arr[i][j-1] + 1
                    arr[i-1][j] = arr[i-1][j] + 1
                    arr[i][j+1] = arr[i][j+1] + 1
                    arr[i][j] = arr[i][j] - MAX
                    continue

        Print(a)
                    
while True:
    if(checkIfMaxElement(a, MAX)):
        sandPile(a, MAX)
        break
    else:
        Print(a)
        break
