import turtle
import random as rand


def trifact(n):
    A0, B0 = 0, 0
    A1, B1 = -250, -250
    A2, B2 = 0, 500
    A3, B3 = 250, -250
    N = [1, 2, 3, 4, 5, 6]
    p = turtle.Turtle()
    p.shape('circle')
    p.shapesize(0.5)
    p.speed('fastest')
    window = turtle.Screen()
    window.screensize(1000, 1000)
    p.penup()
    p.goto(A0, B0)

    for i in range(n):
        x = rand.choice(N)
        if (x == 1 or x == 2):
            p.penup()
            p.goto((p.xcor() + A1)/2, (p.ycor() + B1)/2)
            p.stamp()

        elif (x == 3 or x == 4):
            p.penup()
            p.goto((p.xcor() + A2)/2, (p.ycor() + B2)/2)
            p.stamp()

        elif (x == 5 or x == 6):
            p.penup()
            p.goto((p.xcor() + A3)/2, (p.ycor() + B3)/2)
            p.stamp()
    turtle.done()

trifact(10000)

