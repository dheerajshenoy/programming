import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("IDEA.csv", parse_dates=[0], squeeze=True)
df.drop('Volume', axis=1, inplace=True)
df['MA'] = df['High'].rolling(window=15).mean()
p = np.polyfit(df['MA'], df['High'], deg=2)
f = np.polyval(p, df['Low'])
plt.plot(df['Date'], f, 'y')
plt.plot(df['Date'], df['High'], 'g', alpha=0.4)
plt.plot(df['Date'], df['Low'], 'b', alpha=0.4)
plt.plot(df['Date'], df['MA'], 'r', alpha=0.4)
plt.show()
